package com.itau.maratona;

import com.itau.maratona.entidades.Aluno;
import com.itau.maratona.entidades.Equipe;
import com.itau.maratona.entradasaida.LeitorAlunos;
import com.itau.maratona.entradasaida.LeitorConsole;
import com.itau.maratona.entradasaida.LeitorCsv;
import com.itau.maratona.entradasaida.Impressora;
import com.itau.maratona.servicos.CriadorEquipes;

import java.util.List;

public class App {
	public static void main(String[] args) {
		LeitorAlunos leitor = obterLeitor(args);
		List<Aluno> alunos = leitor.ler();

		List<Equipe> equipes = CriadorEquipes.construir(alunos, 3);

		Impressora.imprimir(equipes);
	}

	private static LeitorAlunos obterLeitor(String[] args){
		if(args.length > 0 && args[0].equals("console")){
			return new LeitorConsole(6);
		}else{
			return new LeitorCsv("src/alunos.csv");
		}
	}
}
