package com.itau.maratona.entidades;

import com.itau.maratona.entidades.Aluno;

import java.util.ArrayList;
import java.util.List;

public class Equipe {
	public int id;
	public List<Aluno> alunos;

	public Equipe() {
		alunos = new ArrayList<>();
	}

	@Override
	public String toString() {
		String texto = "Equipe " + id + ":\n";

		for(Aluno aluno : alunos) {
			texto += aluno;
			texto += "\n";
		}

		return texto;
	}
}
