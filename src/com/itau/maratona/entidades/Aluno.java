package com.itau.maratona.entidades;

public class Aluno {
	private String nome;

	public Aluno(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public String toString() {
		return nome;
	}
}
