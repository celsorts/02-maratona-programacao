package com.itau.maratona.entradasaida;

import java.util.List;

public class Impressora {
	public static void imprimir(List<?> objetos) {
		for(Object objeto : objetos) {
			imprimir(objeto);
		}
	}

	public static void imprimir(Object objeto){
		System.out.println(objeto);
	}
}
