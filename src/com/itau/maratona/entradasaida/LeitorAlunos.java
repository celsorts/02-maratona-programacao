package com.itau.maratona.entradasaida;

import com.itau.maratona.entidades.Aluno;

import java.util.List;

public interface LeitorAlunos {
    public List<Aluno> ler();
}
