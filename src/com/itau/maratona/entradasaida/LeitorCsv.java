package com.itau.maratona.entradasaida;

import com.itau.maratona.entidades.Aluno;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LeitorCsv implements LeitorAlunos{
	private Path caminho;

	public LeitorCsv(String caminho) {
		this.caminho = Paths.get(caminho);
	}

	public List<Aluno> ler(){
		List<String> linhas = lerArquivo();
		List<Aluno> alunos = new ArrayList<>();

		for(String linha : linhas) {
			Aluno aluno = new Aluno(linha);
			alunos.add(aluno);
		}

		return alunos;
	}

	private List<String> lerArquivo(){
		List<String> lista;

		try {
			lista = Files.readAllLines(caminho);
		} catch (IOException e) {
			Impressora.imprimir(e.getMessage());
			lista = new ArrayList<>();
		}

		return lista;
	}

}
