package com.itau.maratona.entradasaida;

import com.itau.maratona.entidades.Aluno;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LeitorConsole implements LeitorAlunos{
    private Scanner scanner;
    private int quantidade;

    public LeitorConsole(int quantidade){
        this.quantidade = quantidade;
    }

    public List<Aluno> ler(){
        List<Aluno> alunos = new ArrayList<>();

        scanner = new Scanner(System.in);

        for(int i = 0; i < quantidade; i++){
            alunos.add(lerAluno());
        }

        scanner.close();

        return alunos;
    }

    private Aluno lerAluno(){
        Impressora.imprimir("Digite o nome de um aluno: ");
        String nome = scanner.nextLine();

        return new Aluno(nome);
    }
}
