package com.itau.maratona.servicos;

import com.itau.maratona.entidades.Aluno;
import com.itau.maratona.entidades.Equipe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CriadorEquipes {
	public static List<Equipe> construir(List<Aluno> alunos, int quantidade){
		List<Equipe> equipes = new ArrayList<>();
		Equipe equipe = new Equipe();

		Collections.shuffle(alunos);

		for(Aluno aluno : alunos){
			equipe.alunos.add(aluno);

			if(equipe.alunos.size() == quantidade) {
				equipes.add(equipe);

				equipe = new Equipe();
				equipe.id = equipes.size() + 1;
			}
		}

		if(equipe.alunos.size() > 0){
			equipes.add(equipe);
		}

		return equipes;
	}
}
