# Maratona de Programação

Uma escola será sede de uma maratona de programação, onde diversas equipes irão competir. A lista de alunos competidores está em um arquivo do tipo csv (veja a pasta src). Modele um sistema que faça a leitura da lista e gere uma lista randômica de equipes.

- Para a maratona desse ano, as equipes serão compostas por 3 alunos.
- Os alunos são organizados em equipes de forma completamente randômica. Um aluno não pode pertencer a duas equipes.
- Cada equipe recebe um id numérico sequencial para identificá-la de forma única.
- A lista de uma turma deve ser exibida no console após a execução do programa.

## Um novo requisito

Os alunos serão cadastrados presencialmente na escola conforme sua ordem de chegada. O sistema precisa aceitar o input de nomes pelo console.